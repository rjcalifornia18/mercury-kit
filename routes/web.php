<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\DemoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('/v1/demo')->group(function () {

    Route::get('/ingreso', [DemoController::class, 'loginPage'])
        ->name('ingreso-demo');

    Route::get('/panel-de-control', [DemoController::class, 'mainDashboardPage'])
        ->name('panel-de-control');

    Route::get('/formularios-basicos', [DemoController::class, 'formsPage'])
        ->name('formularios-basicos');

    Route::get('/formularios-avanzados', [DemoController::class, 'advancedFormsPage'])
        ->name('formularios-avanzados');

    Route::get('/validacion-formularios', [DemoController::class, 'formValidationPage'])
        ->name('validacion-formularios');

    Route::get('/botones-formularios', [DemoController::class, 'formButtonsPage'])
        ->name('botones-formularios');

    Route::get('/formulario-multi-pasos', [DemoController::class, 'wizardFormsPage'])
        ->name('formularios-multi-pasos');

});