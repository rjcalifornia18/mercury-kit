<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DemoController extends Controller
{
    public function loginPage(Request $request){
        return view('layouts.public');
    }

    public function mainDashboardPage(Request $request){
        return view('demo.dashboard');
    }

    public function formsPage(Request $request){
        return view('demo.formularios');
    }

    public function advancedFormsPage(Request $request){
        return view('demo.formularios-avanzados');
    }

    public function formValidationPage(Request $request){
        return view('demo.validacion-formularios');
    }

    public function formButtonsPage(Request $request){
        return view('demo.botones-formulario');
    }

    public function wizardFormsPage(Request $request){
        return view('demo.formulario-multi-pasos');
    }
}
