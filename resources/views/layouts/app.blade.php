<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentelella Alela! | </title>

    <!-- Bootstrap -->
    <link href="{{url("/assets/vendors/bootstrap/dist/css/bootstrap.min.css")}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{url("/assets/vendors/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{url("/assets/vendors/nprogress/nprogress.css")}}" rel="stylesheet">
  

    @yield('head-scripts')

    <!-- Custom styling plus plugins -->
    <link href="{{url("/assets/build/css/custom.css")}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">

    
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- sidebar navigation -->
            @include('layouts.sidebar')
        <!-- sidebar navigation -->

        <!-- top navigation -->
            @include('layouts.topbar')
        <!-- top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                @yield('content')
            </div>
        </div>
         <!-- page content -->

        <!-- footer content -->
            @include('layouts.footer')
        <!-- /footer content -->


      </div>

    </div>

     <!-- jQuery -->
     <script src="{{url("/assets/vendors/jquery/dist/jquery.min.js")}}"></script>
     <!-- Bootstrap -->
    <script src="{{url("/assets/vendors/bootstrap/dist/js/bootstrap.bundle.min.js")}}"></script>
     <!-- FastClick -->
     <script src="{{url("/assets/vendors/fastclick/lib/fastclick.js")}}"></script>
     <!-- NProgress -->
     <script src="{{url("/assets/vendors/nprogress/nprogress.js")}}"></script>
    
     @yield('pagespecificscripts')
 
     <!-- Custom Theme Scripts -->
     <script src="{{url("/assets/build/js/custom.min.js")}}"></script>

     

  </body>
</html>
